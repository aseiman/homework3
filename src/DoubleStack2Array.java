import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

public class DoubleStack2Array {

   int lengthOfStack;
   
   double[] list;
   int head;

   
   public static void main (String[] argum) {
 
   }
   
  
   DoubleStack2Array() {
	  this(100);
   }

   
   DoubleStack2Array(int lengthOfStack) {
	  this.lengthOfStack = lengthOfStack;
      this.list = new double[lengthOfStack];
      this.head = -1;
   }

   
   @Override
   public Object clone() throws CloneNotSupportedException {      
      // Teeb esialgsest tagurpidi magasini
      DoubleStack2Array tempDoubleStack1 = new DoubleStack2Array(this.lengthOfStack);
      while (!this.stEmpty()) {
    	  tempDoubleStack1.push(this.pop());
      }
      
      // Teeb tagurpidi magasinist uue tagurpidi magasini, mis on esialgse magasini koopia
      DoubleStack2Array tempDoubleStack2 = new DoubleStack2Array(this.lengthOfStack);
      while (!tempDoubleStack1.stEmpty()) {
    	  this.push(tempDoubleStack1.tos());
    	  tempDoubleStack2.push(tempDoubleStack1.pop());
      }      
      return tempDoubleStack2;
   }

   
   public boolean stEmpty() {
       if (this.head == -1) {
	   return true;
       } else {
	   return false;
       }
   }

   
   public void push (double a) {
      this.head++;
      if (this.head >= this.lengthOfStack) throw new ArrayIndexOutOfBoundsException("Stack head has reached the limit. Stack is too short.");
      this.list[this.head] = a;
   }

   
   public double pop() {
      double temp = this.list[this.head];      
      this.head--;
      return temp;
   } 

   
   public void op (String s) {
      if (s.equals("+")) {
         this.push(this.pop() + this.pop());
      } else if (s.equals("-")) {
	 this.push(-1 * this.pop() + this.pop());
      } else if (s.equals("*")) {
	 this.push(this.pop() * this.pop());
      } else if (s.equals("/")) {    	  
	 this.push(1/this.pop() * this.pop());
      }    		  
   }
  
   
   public double tos() {
      return this.list[this.head];
   }

   @Override
   public boolean equals (Object o) {
       // Kui objekt pole DoubleStack, siis nad pole ka võrdsed
       if (!(o instanceof DoubleStack2Array)) { 
	   return false;		   
       }

       // Teeb objektist o koopia
       DoubleStack2Array tempDoubleStack1, tempDoubleStack2;
       tempDoubleStack1 = (DoubleStack2Array) o;   
       try {
	   tempDoubleStack2 = (DoubleStack2Array) tempDoubleStack1.clone();
       } catch (CloneNotSupportedException e) {
	   return false;
       }

       // Teeb iseendast koopia
       try {
	   tempDoubleStack1 = (DoubleStack2Array) this.clone();
       } catch (CloneNotSupportedException e) {
	   return false;
       }

       // Võrdleb loodud koopiate elemente omavahel, kuni vähemalt üks magasin tühjaks saab
       while (!tempDoubleStack1.stEmpty() && !tempDoubleStack2.stEmpty()) {
	   double temp1 = tempDoubleStack1.pop();
	   double temp2 = tempDoubleStack2.pop();
	   
	   // Kui mõni elementide paar ei ole võrdne, siis pole ka võrdsed magasinid
	   if (temp1 != temp2) return false; 
       }

       // Kui ühes magasinis on elemente järgi, siis see on pikem kui teine
       // ja seega pole magasinid võrdsed.
       if (!tempDoubleStack1.stEmpty() || !tempDoubleStack2.stEmpty()) {
	   return false;
       }		
       
       // Kõigil muudel juhtudel on magasinid võrdsed
       return true;
   }

   @Override
   public String toString() {	  
       String out = " ";	   
       try {
	  // Teeb magasinist koopi aja liidab koopia elemendid üksteise otsa ühte sõnesse
	  DoubleStack2Array tempDoubleStack1 = (DoubleStack2Array) this.clone();
	  while (!tempDoubleStack1.stEmpty()) {
	     out = Double.toString(tempDoubleStack1.pop()) + " " + out;
	  }
	  out = out.substring(0, out.length() - 1);		 
       } catch (CloneNotSupportedException e) {
	  // TODO Auto-generated catch block
	  e.printStackTrace();
       }
       return out;	  
   }

   public static double interpret (String pol) throws RuntimeException {

       DoubleStack2Array tempDoubleStack = new DoubleStack2Array();

       // Asendab "\t" (tab) sümbolid tavalise tühikuga
       pol.replaceAll("\\t", "\\s");
       
       // Lõikab sõne tühikute kaupa juppideks
       String[] splitted = pol.split("\\s+");

       int j=0;
       int checkInt = 0;
       int checkOps = 0;
       
       while (j < splitted.length) {
	   // Kui tühi sõne, siis liigub järgmise juurde
	   if (splitted[j].equals("")) {
	       j++;
	       continue;
	       
	   // Kui sõne vastab mõnele tehtemärgile (+-*/), siis teostatkse tehe		   
	   } else if (
		   splitted[j].equals("+") || 
		   splitted[j].equals("-") ||
		   splitted[j].equals("*") ||
		   splitted[j].equals("/")
		   ) {			   
	       tempDoubleStack.op(splitted[j]);
	       checkOps++;

	   // Muudel juhtudel peaks olema tegemist arvuga ja vastav arv asetatakse magasini
	   } else {
	       tempDoubleStack.push(Double.parseDouble(splitted[j]));			   
	       checkInt++;
	       
	   }
	   j++;		  
       }	  

       // Kontrollib kas sõnes oli arve ühe võrra rohkem kui tehtemärke
       if (checkInt - checkOps != 1) {
	   throw new RuntimeException("Problems with interpretation of a string: " + pol + ". Please check taht provided string is in correct Reverse Polish notation");
       }

       return tempDoubleStack.tos();	   
   }
}