import java.util.LinkedList;
import java.util.List;


public class DoubleStack {
   
   private List<Double> list;
   
   public static void main (String[] argum) {	   
	   System.out.println(interpret("	22 6 5 9 - + - +"));
   }
       
   
   
   DoubleStack() {	  
      this.list = new LinkedList<Double>();
   }
   
   
   
   @Override
   public Object clone() throws CloneNotSupportedException {      	   
	   DoubleStack clonedDoubleStack = new DoubleStack();
	   for (int i=0; i< this.list.size(); i++) {
		   clonedDoubleStack.push(this.list.get(i));
	   }
	   return clonedDoubleStack;
   }
   

   
   public boolean stEmpty() {
       if (this.list.size() > 0) {
    	   return false;
       } else {
    	   return true;
       }
   }

   
   
   public void push (double a) {      
      this.list.add(a);
   }

   
   
   public double pop() {
	  if (this.list.size() < 1) throw new RuntimeException("Stack under flow. Stack is already empty.");
	  
      double temp = this.list.get(this.list.size() - 1);
      this.list.remove(this.list.size() - 1);
      return temp;
   } 

   
   
   public void op (String s) {
	   if (this.list.size() <= 1) throw new RuntimeException("Stack under flow. Stack does not contain enough operands.");
	   
	   if (s.equals("+")) {
		   this.push(this.pop() + this.pop());
	   } else if (s.equals("-")) {
		   this.push(-1 * this.pop() + this.pop());
	   } else if (s.equals("*")) {
		   this.push(this.pop() * this.pop());
	   } else if (s.equals("/")) {    	  
		   this.push(1/this.pop() * this.pop());
	   } else {
		   throw new RuntimeException("Operation sign not recognised. Please check operation sign: " + s);
	   }
   }
  
   
   
   public double tos() {
	  if (this.list.size() < 1) throw new RuntimeException("Stack under flow. Stack is empty.");
	  
      return this.list.get(this.list.size() - 1);
   }
   
   
   
   public int getHead() {
	   return this.list.size() - 1;
   }

   
   
   @Override
   public boolean equals (Object o) {
       // Kui objekt pole DoubleStack, siis nad pole ka v6rdsed
       if (!(o instanceof DoubleStack)) { 
	      return false;		   
       }

       // Teeb klooni, sest ilma kloonimata 
       DoubleStack tempDoubleStack;
       tempDoubleStack = (DoubleStack) o;   
       
       // Kui objektid pole v6rdse pikkusega, siis pole nad ka v6rdsed
       if (this.list.size() - 1 != tempDoubleStack.getHead()) {
          return false;
       }
       
       // V6rdleb loodud koopiate elemente omavahel, kuni v2hemalt yks magasin tyhjaks saab
       for (int i = this.list.size() - 1; i >= 0; i--) {
          double temp1 = this.list.get(i);
    	  double temp2 = tempDoubleStack.list.get(i);
	   
	      // Kui m6ni elementide paar ei ole v6rdne, siis pole ka v6rdsed magasinid
	      if (temp1 != temp2) return false; 
       }       	
       
       // K6igil muudel juhtudel on magasinid v6rdsed
       return true;
   }

   
   
   @Override
   public String toString() {	  
	   StringBuffer out = new StringBuffer(" ");	   	   
	   for (int i=0;i<this.list.size();i++) {
			   out.append(Double.toString(this.list.get(i)));
			   out.append(" ");
	   }		
	   return out.substring(0, out.length() - 1).toString();	  
   }

   
   public static double interpret (String pol) throws RuntimeException {

       DoubleStack tempDoubleStack = new DoubleStack();

       // Asendab "\t" (tab) symbolid tavalise tyhikuga
       pol.replaceAll("\\t", "\\s");
        
       // L6ikab s6ne tyhikute kaupa juppideks
       String[] splitted = pol.split("\\s+");
       
       int j=0;
       int nOperands = 0;
       int nOperators = 0;
       for (j = 0;j < splitted.length;j++) {
    	   if (splitted[j].equals("")) {
    		   continue;  
    	   } else if (splitted[j].equals("+") || splitted[j].equals("-") || splitted[j].equals("*") || splitted[j].equals("/") ) {
    		   nOperators++;
    	   } else {
    		   nOperands++;
    	   }
       }
       
       if (nOperands <= nOperators) {
    	   throw new RuntimeException("Too many operators and too few operands: " + pol); 
       };
       
             
       j = 0;
       while (j < splitted.length) {
	   // Kui tyhi s6ne, siis liigub j2rgmise juurde
	   if (splitted[j].equals("")) {
	       j++;
	       continue;
	       
	   // Kui s6ne vastab m6nele tehtem2rgile (+-*/), siis teostatkse tehe		   
	   } else if (splitted[j].equals("+") || splitted[j].equals("-") || splitted[j].equals("*") || splitted[j].equals("/") ) {
		   try {
			   tempDoubleStack.op(splitted[j]);
		   }
		   catch (IndexOutOfBoundsException e) {
			   throw new RuntimeException("Stack under flow. Too many operations in notation: " + pol);		   
		   }
		   
	   // Muudel juhtudel peaks olema tegemist arvuga ja vastav arv asetatakse magasini
	   } else {
		   try {
			   tempDoubleStack.push(Double.parseDouble(splitted[j]));			   
		   } catch (Exception e) {
			   throw new RuntimeException("Unrecognised symbol in notation: " + pol);		 
		   }
	       
	   }
	   j++;		  
       }	  

       // Kontrollib kas s6nes oli arve yhe v6rra rohkem kui tehtem2rke
       if (tempDoubleStack.list.size() > 1) {
    	   throw new RuntimeException("Stack is not empty. Too many operands in notation: " + pol);
       }
       
       return tempDoubleStack.tos();	   
   }   
}